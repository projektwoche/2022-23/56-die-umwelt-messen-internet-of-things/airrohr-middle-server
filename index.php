<?php
require('config.php');
require __DIR__ . '/vendor/autoload.php';
use InfluxDB2\Client;
use InfluxDB2\Point;

$influx_client = new Client([
  "url" => INFLUX_HOST,
  "token" => INFLUX_TOKEN,
  "bucket" => INFLUX_BUCKET,
  "org" => INFLUX_ORG,
  "precision" => InfluxDB2\Model\WritePrecision::S,
  "debug" => false
]);

function checkAuth() {
  if(isset($_SERVER["HTTP_X_API_TOKEN"])) {
    $token = $_SERVER['HTTP_X_API_TOKEN'];
    return TOKEN_HASH == hash('sha256', $token);
  } else {
    return false;
  }
}
function sendDataToInflux(float $temperature, float $humidity, float $pm10, float $pm25, string $sensor_id ,$client) {
  $writeApi = $client->createWriteApi();
  $dateTimeNow = new DateTime('NOW');
  $point = Point::measurement("measurement")
        ->addTag("sensorid", $sensor_id)
        ->addField("pm10", $pm10)
        ->addField("pm25", $pm25)
        ->addField("temperature", $temperature)
        ->addField("humidity", $humidity)
        ->time($dateTimeNow->getTimestamp());
  $writeApi->write($point);
  $writeApi->close();
}
function sendStatusToInflux(int $status_code, string $data, string $sensor_id ,$client) {
  $writeApi = $client->createWriteApi();
  $dateTimeNow = new DateTime('NOW');
  $point = Point::measurement("status")
        ->addTag("sensorid", $sensor_id)
        ->addTag("statuscode", strval($status_code))
        ->addField("data", $data)
        ->time($dateTimeNow->getTimestamp());
  $writeApi->write($point);
  $writeApi->close();
}
function sendDataToSensorCommunity(float $temperature, float $humidity, float $pm10, float $pm25, string $dht22pin, string $sds011pin, string $apiEspId, string $apiUserAgent, string $apiEndpoint) {
  $curl_handle = curl_init();
  curl_setopt($curl_handle, CURLOPT_URL, $apiEndpoint);
  curl_setopt($curl_handle, CURLOPT_POST, 1);
  $data1 = array('software_version'=>$apiUserAgent, 'sensordatavalues'=>array(
    array('value_type'=>'P1', 'value'=>strval($pm10)),
    array('value_type'=>'P2', 'value'=>strval($pm25))
    ));
  $data2 = array('software_version'=>$apiUserAgent, 'sensordatavalues'=>array(
    array('value_type'=>'temperature', 'value'=>strval($temperature)),
    array('value_type'=>'humidity', 'value'=>strval($humidity))
    ));
  curl_setopt( $curl_handle, CURLOPT_POSTFIELDS, json_encode($data1));
  curl_setopt($curl_handle, CURLOPT_HTTPHEADER, [
      "Content-Type: application/json",
      "X-Sensor: {$apiEspId}",
      "X-Pin: {$sds011pin}"
  ]);
  $res1 = curl_exec($curl_handle);
  curl_setopt( $curl_handle, CURLOPT_POSTFIELDS, json_encode($data2));
  curl_setopt($curl_handle, CURLOPT_HTTPHEADER, [
      "Content-Type: application/json",
      "X-Sensor: {$apiEspId}",
      "X-Pin: {$dht22pin}"
  ]);
  $res2 = curl_exec($curl_handle);
  curl_close($curl_handle);
  return array($res1, $res2);
}
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  include 'public.html';
} elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if (checkAuth()) {
    if ($_REQUEST['type'] == 'data') {
      $data = json_decode(file_get_contents('php://input'), true);
      if (isset($data['sensor_id']) and isset($data['temperature']) and isset($data['humidity']) and isset($data['pm10']) and isset($data['pm25'])) {
        $sensor_id = $data['sensor_id'];
        $temperature = $data['temperature'];
        $humidity = $data['humidity'];
        $pm10 = $data['pm10'];
        $pm25 = $data['pm25'];
        sendDataToInflux($temperature, $humidity, $pm10, $pm25, $sensor_id ,$influx_client);
        sendDataToSensorCommunity($temperature, $humidity, $pm10, $pm25, 7, 1, $sensor_id, "php-cli", "https://api.sensor.community/v1/push-sensor-data/");
      } else {
        print('at least one required value was not given');
        http_response_code(400);
      }
    } elseif ($_REQUEST['type'] == 'status') {
      $data = json_decode(file_get_contents('php://input'), true);
      if (isset($data['sensor_id']) and isset($data['statuscode']) and isset($data['data'])) {
        $sensor_id = $data['sensor_id'];
        $statuscode = $data['statuscode'];
        $data = $data['data'];
        sendStatusToInflux($statuscode, $data, $sensor_id ,$influx_client);
      } else {
        print('at least one required value was not given');
        http_response_code(400);
      }
    } else {
      print("No action was given");
      http_response_code(400);
    }
  } else {
    http_response_code(401);
  }
} else {
  http_response_code(405);
}
?>
